export function ClassEnhancer(OriginalClass) {
  return class EnhancedClass extends OriginalClass {
    constructor(props) {
      super(props);
      this.enhancedProp = 'foo';
    }
    enhancedFunc() {
      return this.enhancedProp;
    }
  };
}
