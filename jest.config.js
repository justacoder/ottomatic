module.exports = {
  rootDir: './src',
  testEnvironment: 'jsdom',
  testPathIgnorePatterns: [
    '<rootDir>/.idea',
    '<rootDir>/node_modules',
    '<rootDir>/coverage',
  ],
  globals: {},
  collectCoverage: true,
  collectCoverageFrom: ['<rootDir>/**/*.{js,jsx,mjs}'],
  coverageDirectory: '<rootDir>/coverage',
  coverageReporters: ['html', 'json', 'lcov', 'cobertura', 'text-summary'],
  coverageThreshold: {},
};
